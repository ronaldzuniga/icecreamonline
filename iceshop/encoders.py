from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import QuerySet

from .models import Flavor, Topping


class BaseEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, QuerySet):
            return list(obj)

        return super().default(obj)


class FlavorEncoder(BaseEncoder):
    def default(self, obj):
        if isinstance(obj, Flavor):
            return {'id': obj.id, 'name': obj.name}

        return super().default(obj)


class ToppingEncoder(BaseEncoder):
    def default(self, obj):
        if isinstance(obj, Topping):
            return {'id': obj.id, 'name': obj.name}

        return super().default(obj)
