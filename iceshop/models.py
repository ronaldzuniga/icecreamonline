# -*- coding: utf-8 -*-

from django.db import models


class Topping(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Flavor(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Order(models.Model):
    toppings = models.ManyToManyField(Topping)
    flavors = models.ManyToManyField(Flavor)
    username = models.CharField(max_length=255)
    email = models.EmailField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        toppings = ', '.join([t.name for t in self.toppings])
        flavors = ', '.join([f for f in self.flavors])
        return '{}:{} -> Flavors: {} Toppins: {}'.\
            format(self.username, self.email,
                   flavors, toppings)
