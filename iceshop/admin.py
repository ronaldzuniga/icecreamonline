# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Order, Flavor, Topping

admin.site.register(Order)
admin.site.register(Flavor)
admin.site.register(Topping)
