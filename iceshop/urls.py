from django.urls import path
from django.views.generic import TemplateView

from .views import order, flavor, topping

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html")),
    path('flavor', flavor, name='flavor'),
    path('topping', topping, name='topping'),
    path('order', order, name='order')
]
