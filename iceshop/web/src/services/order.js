import axios from 'axios';
import * as flavorActions from '../store/actions/flavors';
import * as toppingActions from '../store/actions/toppings';

export const fetchFlavors = (dispatch) => {
    dispatch(flavorActions.flavorsFetchInit());
    axios.get('flavor')
        .then((response) => dispatch(flavorActions.flavorsFetchSuccess(response.data)))
        .catch((error) => dispatch(flavorActions.flavorsFetchError(error)));
};

export const fetchToppings = (dispatch) => {
    dispatch(toppingActions.toppingsFetchInit());
    axios.get('topping')
        .then((response) => dispatch(toppingActions.toppingsFetchSuccess(response.data)))
        .catch((error) => dispatch(toppingActions.toppingsFetchError(error)));
};