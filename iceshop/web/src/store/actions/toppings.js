export const TOPPINGS_FETCH_INIT = 'TOPPINGS_FETCH_INIT';
export const TOPPINGS_FETCH_SUCCESS = 'TOPPINGS_FETCH_SUCCESS';
export const TOPPINGS_FETCH_ERROR = 'TOPPINGS_FETCH_ERROR';

export const toppingsFetchInit = () => ({
    type: TOPPINGS_FETCH_INIT,
    isLoading: true,
});

export const toppingsFetchSuccess = (toppings) => ({
    type: TOPPINGS_FETCH_SUCCESS,
    toppings,
});

export const toppingsFetchError = (error) => ({
    type: TOPPINGS_FETCH_ERROR,
    error,
});

