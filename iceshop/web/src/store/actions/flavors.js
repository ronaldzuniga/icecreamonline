export const FLAVORS_FETCH_INIT = 'FLAVORS_FETCH_INIT';
export const FLAVORS_FETCH_SUCCESS = 'FLAVORS_FETCH_SUCCESS';
export const FLAVORS_FETCH_ERROR = 'FLAVORS_FETCH_ERROR';

export const flavorsFetchInit = () => ({
    type: FLAVORS_FETCH_INIT,
    isLoading: true,
});

export const flavorsFetchSuccess = (flavors) => ({
    type: FLAVORS_FETCH_SUCCESS,
    flavors,
});

export const flavorsFetchError = (error) => ({
    type: FLAVORS_FETCH_ERROR,
    error,
});

