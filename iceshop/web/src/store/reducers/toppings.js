import * as toppingActions from '../actions/toppings';

export const toppingReducer = (state={}, action) => {
    switch (action.type) {
        case toppingActions.TOPPINGS_FETCH_INIT:
            return { isLoading: action.isLoading };
        case toppingActions.TOPPINGS_FETCH_ERROR:
            return { error: action.error };
        case toppingActions.TOPPINGS_FETCH_SUCCESS:
            return { toppings: action.toppings };
        default:
            return state;
    }
};