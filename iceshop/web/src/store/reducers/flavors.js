import * as flavorActions from '../actions/flavors';

export const flavorReducer = (state={}, action) => {
    switch (action.type) {
        case flavorActions.FLAVORS_FETCH_INIT:
            return { isLoading: action.isLoading };
        case flavorActions.FLAVORS_FETCH_ERROR:
            return { error: action.error };
        case flavorActions.FLAVORS_FETCH_SUCCESS:
            return { flavors: action.flavors.flavors };
        default:
            return state;
    }
};