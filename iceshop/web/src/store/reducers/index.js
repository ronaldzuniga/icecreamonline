import { combineReducers } from 'redux';
import { flavorReducer } from './flavors';
import { toppingReducer } from './toppings';

export default combineReducers({
    flavors: flavorReducer,
    toppings: toppingReducer,
});
