import React, { Component } from 'react';
import logo from './logo.svg';

import ShoppingPage from './components/shopping/ShoppingPage';

class App extends Component {
  render() {
    return (
      <ShoppingPage />
    );
  }
}

export default App;
