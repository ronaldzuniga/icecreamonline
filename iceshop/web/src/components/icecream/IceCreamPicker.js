import React, { Component } from 'react';

import { FormControl } from 'material-ui/Form';
import { MenuItem } from 'material-ui/Menu';
import { ListItemText } from 'material-ui/List';
import Input, { InputLabel } from 'material-ui/Input';
import Select from 'material-ui/Select';
import Checkbox from 'material-ui/Checkbox';

export default class IceCreamPicker extends Component {
    constructor() {
        super();
        this.state = { flavors: [], toppings: [] };
    }

    handleFlavorChange = event => {
        this.setState({ ...this.state, flavors: event.target.value });
    }

    handleToppingChange = event => {
        this.setState({ ...this.state, toppings: event.target.value });
    }

    renderOptions = (allOptions, selected) => {
        return allOptions && allOptions.map(option => (
          <MenuItem key={ option.name } value={ option.id }>
            <Checkbox checked={ !!selected.find(id => id === option.id) } />
            <ListItemText primary={ option.name } />
          </MenuItem>
        ));
    }

    renderSelected = (options) =>
        (selected) =>
            selected.reduce((names, s) => {
                const found = options.find(f => f.id === s);
                found && names.push(found.name);
                return names;
            }, []).join(', ');

    render() {
        const { flavors, toppings } = this.props;
        console.log(this.props);
        return (
            <div>
                <FormControl>
                  <InputLabel htmlFor="select-multiple-flavors">Flavors</InputLabel>
                  <Select
                    multiple
                    value={ this.state.flavors }
                    onChange={ this.handleFlavorChange }
                    input={ <Input id="select-multiple-flavors" /> }
                    renderValue={ this.renderSelected(flavors) }>
                        { this.renderOptions(this.props.flavors, this.state.flavors) }
                  </Select>
                </FormControl>
                <FormControl>
                  <InputLabel htmlFor="select-multiple-toppings">Toppings</InputLabel>
                  <Select
                    multiple
                    value={ this.state.toppings }
                    onChange={ this.handleToppingChange }
                    input={ <Input id="select-multiple-toppings" /> }
                    renderValue={ this.renderSelected(toppings) }>
                        { this.renderOptions(this.props.toppings, this.state.toppings) }
                  </Select>
                </FormControl>
            </div>
        );
    }

}