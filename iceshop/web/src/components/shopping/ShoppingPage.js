import React, { Component } from 'react';
import { connect } from 'react-redux';

import { CircularProgress } from 'material-ui/Progress';
import Snackbar from 'material-ui/Snackbar';

import ShoppingForm from './ShoppingForm';

import * as orderService from '../../services/order';


class ShoppingPage extends Component {

    componentDidMount() {
        this.props.fetchFlavors && this.props.fetchFlavors();
        this.props.fetchToppings && this.props.fetchToppings();
    }

    render() {
        const { isLoading, error, flavors, toppings } = this.props;
        return (
            <div>
                { isLoading && <CircularProgress /> }
                { error && <Snackbar open={ true } message={ error.message } />}
                { flavors && toppings && <ShoppingForm flavors={ flavors } toppings={ toppings } /> }
            </div>
        );
    }
}

export default connect(
    state => ({
        isLoading: state.flavors.isLoading || state.toppings.isLoading,
        error: state.flavors.error || state.toppings.error,
        flavors: state.flavors.flavors,
        toppings: state.toppings.toppings,
    }),
    dispatch => ({
        fetchFlavors: () => orderService.fetchFlavors(dispatch),
        fetchToppings: () => orderService.fetchToppings(dispatch),
    }),
)(ShoppingPage);