import React, { Component } from 'react';

import ContactInformation from '../contact/ContactInformation';
import IceCreamPicker from '../icecream/IceCreamPicker';


export default class ShoppingForm extends Component {

  render() {
    const { flavors, toppings } = this.props;

    return (
      <form>
        <ContactInformation />
        <IceCreamPicker flavors={ flavors } toppings={ toppings } />
      </form>
    );
  }
}