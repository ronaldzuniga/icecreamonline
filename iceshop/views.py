# -*- coding: utf-8 -*-
from django.http import JsonResponse, HttpResponseBadRequest

from .encoders import FlavorEncoder, ToppingEncoder
from .models import Order, Flavor, Topping


def flavor(request):
    flavors = Flavor.objects.all()

    return JsonResponse({
        'flavors': flavors,
    }, FlavorEncoder)


def topping(request):
    toppings = Topping.objects.all()

    return JsonResponse({
        'topings': toppings,
    }, ToppingEncoder)


def order(request):
    if request.method != 'POST':
        return HttpResponseBadRequest()
    name = request.POST.get('name')
    email = request.POST.get('email')
    toppings = request.POST.get('toppings')
    flavors = request.POST.get('flavors')

    new_order = Order(name=name, email=email)
    for t in toppings:
        order.toppings.add(t)
    for f in flavors:
        order.flavors.add(f)

    new_order.save()
