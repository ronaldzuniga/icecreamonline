from iceshop.models import Flavor, Topping

for line in open('flavors.txt'):
    f = Flavor(name=line.strip())
    f.save()

for line in open('toppings.txt'):
    t = Topping(name=line.strip())
    t.save()
