-- Get orders per day
select created::date as order_date, count(*) total_orders from iceshop_order group by 1;

-- Get orders by flavor
select FLAVOR.name as flavor, count(ICECREAM_ORDER.id) as orders
from "iceshop_order" ICECREAM_ORDER
left join "iceshop_order_flavors" ORDER_FLAVOR on ICECREAM_ORDER.id = ORDER_FLAVOR.order_id
left join iceshop_flavor FLAVOR on FLAVOR.id = ORDER_FLAVOR.flavor_id 
group by 1
